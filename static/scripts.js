function showSuccessAlert() {
    alert("Data added successfully!");
    return true; // Allow form submission
}

function showDeleteAlert() {
    alert("Data deleted successfully!");
    return true; // Allow form submission
}

function retrieveData() {
    const client = document.getElementById("retrieve_client").value;
    const agency = document.getElementById("retrieve_agency").value;
    const location = document.getElementById("retrieve_location").value;

    // Send an AJAX request to retrieve data
    fetch(`/retrieve_data?client=${client}&agency=${agency}&location=${location}`)
        .then(response => response.json())
        .then(data => {
            // Handle the retrieved data and update the table
            const tableBody = document.querySelector("table tbody");
            tableBody.innerHTML = ""; // Clear previous data

            if (data.length === 0) {
                tableBody.innerHTML = "<tr><td colspan='5'>No matching data found.</td></tr>";
            } else {
                data.forEach(item => {
                    const newRow = document.createElement("tr");
                    newRow.innerHTML = `
                        <td>${item.client}</td>
                        <td>${item.agencies[0].agency}</td>
                        <td>${item.agencies[0].locations.length > 0 ? item.agencies[0].locations.map(location => location.location).join('<br>') : ''}</td>
                        <td>${item.agencies[0].locations.length > 0 ? item.agencies[0].locations[0].department : ''}</td>
                        <td>${item.agencies[0].locations.length > 0 ? (item.agencies[0].locations[0].active === 1 ? "Active" : "Inactive") : ''}</td>
                    `;
                    tableBody.appendChild(newRow);
                });
            }
        })
        .catch(error => {
            console.error(error);
            const tableBody = document.querySelector("table tbody");
            tableBody.innerHTML = "<tr><td colspan='5'>Error retrieving data.</td></tr>";
        });
}