from flask import Flask
from flask_pymongo import PyMongo
from pymongo import MongoClient

app = Flask(__name__, static_url_path='/static')
app.secret_key = 'RizeMonitoring'

# Set the MONGO_URI in the Flask configuration
app.config["MONGO_URI"] = "mongodb+srv://admin:admin123@zuitt.sknrqtl.mongodb.net/rize.accounts?retryWrites=true&w=majority"

# Initialize PyMongo with the Flask app
mongo = PyMongo(app)

# Access the 'rize' database (this will create it if it doesn't exist)
db = mongo.db

# Access the 'accounts' collection (this will create it if it doesn't exist)
collection = db["accounts"]

@app.teardown_appcontext
def close_mongo_client(error):
    mongo.cx.close()

if __name__ == "__main__":
    app.run(debug=True)