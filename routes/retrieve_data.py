from flask import Blueprint, request, jsonify
from models.models import Client

retrieve_data_bp = Blueprint('retrieve_data', __name__)

@retrieve_data_bp.route("/retrieve_data", methods=["GET"])
def retrieve_data():
    client_name = request.args.get("client")
    agency_name = request.args.get("agency")
    location_name = request.args.get("location")

    query = {}

    if client_name:
        query["client"] = client_name

    if agency_name or location_name:
        query["$or"] = [{"agencies.agency": agency_name}, {"agencies.locations.location": location_name}]

    retrieved_data = Client.objects(**query)

    retrieved_data = [item.to_mongo().to_dict() for item in retrieved_data]

    return jsonify(retrieved_data)

# Register the blueprint within the same file
from . import app  # Import app from the current package
app.app.register_blueprint(retrieve_data_bp)