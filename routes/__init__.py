from .index import index_bp
from .add_data import add_data_bp
from .delete_data import delete_data_bp
from .retrieve_data import retrieve_data_bp