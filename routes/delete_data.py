from flask import Blueprint, request, redirect, url_for, flash
from models.models import Client

delete_data_bp = Blueprint('delete_data', __name__)

@delete_data_bp.route("/delete_data", methods=["POST"])
def delete_data():
    try:
        client_name = request.form.get("client")
        agency_name = request.form.get("agency")
        location_name = request.form.get("location")

        if not (client_name or agency_name or location_name):
            flash("At least one of Client, Agency, or Location is required for deletion", "warning")
            return redirect(url_for("index"))

        query = {}
        if client_name:
            query["client"] = client_name
        if agency_name:
            query["agencies.agency"] = agency_name
        if location_name:
            query["agencies.locations.location"] = location_name

        deleted_count = Client.objects(**query).delete()

        if deleted_count > 0:
            flash("Data deleted successfully", "success")
        else:
            flash("No matching data found for deletion", "info")
    except Exception as e:
        flash(f"Error deleting data: {str(e)}", "error")

    return redirect(url_for("index"))

# Register the blueprint within the same file
from . import app  # Import app from the current package
app.app.register_blueprint(delete_data_bp)