from flask import Blueprint, request, redirect, url_for, flash
from models.models import Client, Agency, Location

add_data_bp = Blueprint('add_data', __name__)

@add_data_bp.route("/add_data", methods=["POST"])
def add_data():
    try:
        client_name = request.form["client"]
        agency_name = request.form["agency"]
        location_name = request.form.get("location")
        department_name = request.form.get("department")
        status = request.form["status"]

        active = 1 if status == "active" else 0

        client = Client.objects(client=client_name).first() or Client(client=client_name)
        agency = next((a for a in client.agencies if a.agency == agency_name), None) or Agency(agency=agency_name)

        if location_name:
            location = Location(location=location_name, department=department_name, active=active)
            agency.locations.append(location)

        client.agencies.append(agency)
        client.save()

        flash("Data added successfully", "success")
    except Exception as e:
        flash(f"Error adding data: {str(e)}", "error")

    return redirect(url_for("index"))

# Register the blueprint within the same file
from . import app  # Import app from the current package
app.app.register_blueprint(add_data_bp)