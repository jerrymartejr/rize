from flask import Blueprint, render_template
from app import mongo

index_bp = Blueprint('index', __name__)

@index_bp.route("/")
def index():
    try:
        data = mongo.db.clients.find()
        counts = {
            "client_count": sum(1 for client in data),
            "agency_count": sum(len(client.get("agencies", [])) for client in data),
            "location_count": sum(len(agency.get("locations", [])) for client in data for agency in client.get("agencies", []))
        }
        return render_template("index.html", data=data, counts=counts)
    except Exception as e:
        error_message = f"Error accessing MongoDB: {str(e)}"
        print(error_message)  # Add this line for debugging
        return error_message

# Register the blueprint within the same file
from . import app  # Import app from the current package
app.app.register_blueprint(index_bp)